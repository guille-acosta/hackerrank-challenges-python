'''
Sam's house has an apple tree and an orange tree that yield an abundance of fruit. The apple tree is 
to the left of his house, and the orange tree is to its right. You can assume the trees are located on 
a single point, where the apple tree is at point a, and the orange tree is at point b.
When a fruit falls from its tree, it lands  units of distance from its tree of origin along the -axis. 
A negative value of  means the fruitfell  units to the tree's left, and a positive value of  means 
it falls  units to the tree's right.

Given the value of  for  apples and  oranges, determine how many apples and oranges will fall on Sam's 
house (i.e., in the inclusive range [s, t])?

Function Description
Complete the countApplesAndOranges function in the editor below. It should print the number of apples 
and oranges that land on Sam's house, each on a separate line.

countApplesAndOranges has the following parameter(s):
s: integer, starting point of Sam's house location.
t: integer, ending location of Sam's house location.
a: integer, location of the Apple tree.
b: integer, location of the Orange tree.
apples: integer array, distances at which each apple falls from the tree.
oranges: integer array, distances at which each orange falls from the tree.

Input Format
The first line contains two space-separated integers denoting the respective values of s and t.
The second line contains two space-separated integers denoting the respective values of a and b.
The third line contains two space-separated integers denoting the respective values of m and n.
The fourth line contains m space-separated integers denoting the respective distances that each 
apple falls from point a.
The fifth line contains n space-separated integers denoting the respective distances that each 
orange falls from point b.

Constraints
  * 1 <= s,t,a,b,m,n <= 10^5
  * -10^5 <= d <= 10^5
  * a <s < t < b

Output Format
Print two integers on two different lines:
The first integer: the number of apples that fall on Sam's house.
The second integer: the number of oranges that fall on Sam's house.

'''


# s = empieza la casa, t = temina la casa, a = manzano, b = naranjo, apples = lista de distancias a
#la que cae del manzano, orange = lista de distancias a la que cae del naranjo
def countApplesAndOranges(s, t, a, b, apples, oranges):
    apples_dist = [apple + a for apple in apples]
    oranges_dist = [orange + b for orange in oranges]

    num_apples = len([apple for apple in apples_dist if apple >= s and apple <= t])
    num_oranges = len([orange for orange in oranges_dist if orange >= s and orange <= t])

    print("{}\n{}".format(num_apples, num_oranges))
