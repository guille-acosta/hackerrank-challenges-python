'''
HackerLand University has the following grading policy:
Every student receives a  in the inclusive range from  to .
Any  less than  is a failing grade.

Sam is a professor at the university and likes to round each student's  according to these rules:
-If the difference between the grade and the next multiple of 5 is less than 3, round  up to the 
next multiple of 5.
-If the value of grade is less than 38, no rounding occurs as the result will still be a failing grade.
For example, grade = 84 will be rounded to 85 but grade = 29 will not be rounded because the rounding 
would result in a number that is less than 40.

Given the initial value of grade for each of Sam's n students, write code to automate the rounding 
process.

Function Description
Complete the function gradingStudents in the editor below. It should return an integer array consisting of rounded grades.

gradingStudents has the following parameter(s):
grades: an array of integers representing grades before rounding

Input Format
The first line contains a single integer, , the number of students.
Each line i of the n subsequent lines contains a single integer, grades[i], denoting student i's grade.

Constraints
 1 <= n <= 60
 0 <= grades[i] <= 100

Output Format
For each grades[i], print the rounded grade on a new line.
'''

def grading_students(grades:list()) -> list() :
    new_grades = list()
    for grade in grades:
        next_multiple = (grade//5 +1)*5
        if grade < 38 or next_multiple - grade >= 3:
            new_grades.append(grade)

        else:
            new_grades.append(next_multiple)

    return new_grades