'''
Alice and Bob each created one problem for HackerRank. A reviewer rates the two challenges, 
awarding points on a scale from 1 to 100 for three categories: problem clarity,
 originality, and difficulty.

We define the rating for Alice's challenge to be the triplet a = (a[0],a[1],a[2]) , and the rating for Bob's 
challenge to be the triplet b = (b[0],b[],b[1][2])

Your task is to find their comparison points by comparing  with ,  with , and  with .

If a[i] > b[i], then Alice is awarded  point.
If a[i] < b[i] , then Bob is awarded  point.
If a[i] = b[i] , then neither person receives a point.
Comparison points is the total points a person earned.
Given and , determine their respective comparison points.
Function Description

Complete the function compareTriplets in the editor below. It must return an array of two integers, 
the first being Alice's score and the second being Bob's.
compareTriplets has the following parameter(s):

a: an array of integers representing Alice's challenge rating
b: an array of integers representing Bob's challenge rating
'''

def compareTriplets(a, b):
    alice_points = sum([m > n for m, n in zip(a, b)])
    bob_points = sum([p > q for p, q in zip(b, a)])

    return [alice_points, bob_points]
