'''
Function Description
Complete the simpleArraySum function in the editor below. It must return the sum of the array elements
as an integer.
simpleArraySum has the following parameter(s):
ar: an array of integers

'''


def simpleArraySum(ar):
    return sum([integer for integer in ar])
