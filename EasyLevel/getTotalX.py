'''
You will be given two arrays of integers and asked to determine all integers that satisfy the 
following two conditions:

The elements of the first array are all factors of the integer being considered
The integer being considered is a factor of all elements of the second array
These numbers are referred to as being between the two arrays. You must determine how many such 
numbers exist.

For example, given the arrays 'a=[2,6]' and 'b=[24,36]', there are two numbers between them:6  
and 12. '6%2=0', '6%6 = 0', '24%6=0' and '36%6=0' for the first value. Similarly, '12%2 = 0', '12%6'=0  
and 24%12=0, 36%12=0.

Function Description

Complete the getTotalX function in the editor below. It should return the number of integers that 
are betwen the sets.

getTotalX has the following parameter(s):

a: an array of integers
b: an array of integers

Input Format
The first line contains two space-separated integers, n and m, the number of elements in array a and 
the number of elements in array b.
The second line contains n distinct space-separated integers describing  where 0 <= i < n.
The third line contains m distinct space-separated integers describing  where 0 <= j < m.

Constraints
 * 1 <= n,m <= 10
 * 1 <= a[i] <= 100
 * 1 <= b[j] <= 100
 Output Format

Print the number of integers that are considered to be between a and b.

'''
def multiplers(num:int) -> list():
    lit_multiplers = list()

    for j in range(1, 101):
        if j % num == 0:
            lit_multiplers.append(j)
    return lit_multiplers

def divisors(num: int) -> list():
    list_divisors = list()

    for j in range(1, 101):
        if num % j == 0:
            list_divisors.append(j)

    return list_divisors


def getTotalX(a: list(), b: list()) -> int:
    a_multiplers = list()
    b_divisors = list()
    common_a_multiplers = set()
    common_b_divisors = set()

    for j in range(len(a)):
        a_multiplers.append(multiplers(a[j]))

    for k in range(len(b)):
        b_divisors.append(divisors(b[k]))

    
    for m in range(len(a_multiplers)):
        if m == 0:
            common_a_multiplers = set(a_multiplers[m])
        else:
            common_a_multiplers &= set(a_multiplers[m])

    for n in range(len(b_divisors)):
        if n == 0:
            common_b_divisors = set(b_divisors[n])
        else:
            common_b_divisors &= set(b_divisors[n])

    return len(common_a_multiplers & common_b_divisors)
