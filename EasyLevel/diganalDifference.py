'''
Function description

Complete the diagonalDifference function in the editor below. It must return an integer representing the absolute 
diagonal difference.

diagonalDifference takes the following parameter:

arr: an array of integers .
Input Format

The first line contains a single integer, n, the number of rows and columns in the matrix arr.
Each of the next n lines describes a row, arr[i], and consists of n space-separated integers arr[i][j].
'''

def diagonalDifference(arr):
    num_rows = len(arr)

    dig_1 = sum([arr[x][y] for x, y in zip(range(num_rows), range(num_rows))])
    dig_2 = sum([arr[m][n] for m, n in zip(range(num_rows), range(num_rows)[::-1])])

    return abs(dig_1 - dig_2)