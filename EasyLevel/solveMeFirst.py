 '''
    Function prototype:

    solveMeFirst(a:int, b:int) -> int

    where,

    a is the first integer input.
    b is the second integer input
    Return values

    sum of the above two integers
'''

def solveMeFirst(a,b):
	return a + b