'''
Maria plays college basketball and wants to go pro. Each season she maintains a record of her play. She tabulates the number 
of times she breaks her season record for most points and least points in a game. Points scored in the first game establish 
her record for the season, and she begins counting from there.

For example, assume her scores for the season are represented in the array 'scores = [12,24,10,24]'. Scores are in the same 
order as the games played.


Given Maria's scores for a season, find and print the number of times she breaks her records for most and least points scored 
during the season.

Function Description
Complete the breakingRecords function in the editor below. It must return an integer array containing the numbers of times she 
broke her records. Index 0 is for breaking most points records, and index 1 is for breaking least points records.

breakingRecords has the following parameter(s):
scores: an array of integers

The first line contains an integer 'n', the number of games.
The second line contains  'n'space-separated integers describing the respective values of 'score0' , 'score1', ... 'scoren-1'

Sample Input 0: 10 5 20 20 4 5 2 25 1
She broke her best record twice (after games 2 and 7) and her worst record four times (after games 1, 4, 6, and 8), so we print 
2 4 as our answer. Note that she did not break her record for best score during game 3, as her score during that game was not 
strictly greater than her best record at the time

Sample Input 1: 3 4 21 36 10 28 35 5 24 42
She broke her best record four times (after games 1, 2, 3, and 9) and her worst record zero times (no score during the season 
was lower than the one she earned during her first game), so we print 4 0 as our answer.

Contraints:
* 1 <= n <= 1000
* 0 <= scores[i] <= 10^8

'''


def breakingRecords(scores: list()) -> list():
    num_worst_broke = 0
    num_best_broke = 0
    min_score = scores[0]
    max_score = scores[0]

    for score in scores[1:]:                
        if score > max_score:
            max_score = score
            num_best_broke += 1

        if score < min_score:                
            min_score = score
            num_worst_broke += 1
   
    return [num_best_broke, num_worst_broke]
