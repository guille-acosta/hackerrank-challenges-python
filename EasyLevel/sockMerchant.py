'''
John works at a clothing store. He has a large pile of socks that he must pair by color for sale. Given an array of 
integers representing the color of each sock, determine how many pairs of socks with matching colors there are.

For example, there are n=7 socks with colors ar = [1,2,1,2,1,3,2]. There is one pair of color 1 and one of color 2. 
There are three odd socks left, one of each color. The number of pairs is 2.

Function Description
Complete the sockMerchant function in the editor below. It must return an integer representing the number of matching 
pairs of socks that are available.

sockMerchant has the following parameter(s):
n: the number of socks in the pile
ar: the colors of each sock

Input Format
The first line contains an integer n, the number of socks represented in ar.
The second line contains  nspace-separated integers describing the colors ar[i] of the socks in the pile.

Constraints
  * 1 <= n <= 100
  * 1 <= ar[1] <= 100 where 0 <= i < n

Output Format
Return the total number of matching pairs of socks that John can sell.

'''

def sockMerchant(n: int, ar: list()):
    pairs = 0
    ar_ordered = sorted(ar)
    colors = list()
    colors = [ar_ordered[i] for i,j in zip(range(n - 1),range(1, n)) if ar_ordered[i] != ar_ordered[j]]
    if len(colors) != 0:
        if colors[-1] != ar_ordered[-1]:
            colors.append(ar_ordered[-1])
    else:
        colors.append(ar[-1])

    
    for color in colors:
        q = ar_ordered.count(color)//2
        if q != 0:
            pairs += q

    return pairs
