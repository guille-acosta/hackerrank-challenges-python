'''
Marie invented a Time Machine and wants to test it by time-traveling to visit Russia on the Day of the Programmer 
(the 256 day of the year) during a year in the inclusive range from 1700 to 2700.

From 1700 to 1917, Russia's official calendar was the Julian calendar; since 1919 they used the Gregorian calendar 
system. The transition from the Julian to Gregorian calendar system occurred in 1918, when the next day after 
January 31 was February 14. This means that in 1918, February 14 was the 32 day of the year in Russia.

In both calendar systems, February is the only month with a variable amount of days; it has 29 days during a leap 
year, and 28 days during all other years. In the Julian calendar, leap years are divisible by 4; in the Gregorian calendar, 
leap years are either of the following:

Divisible by 400.
Divisible by 4 and not divisible by 100.
Given a year, y, find the date of the 256 day of that year according to the official Russian calendar during that year. 
Then print it in the format dd.mm.yyyy, where dd is the two-digit day, mm is the two-digit month, and 'yyyy' is 'y'.

For example, the given year = 1984.  is divisible by 4, so it is a leap year. The 256 day of a leap year after 1918 is 
September 12, so the answer is 12.09.1984.

Function Description
Complete the dayOfProgrammer function in the editor below. It should return a string representing the date of the  day 
of the year given.

dayOfProgrammer has the following parameter(s):
year: an integer

Input Format
A single integer denoting year y.

Contraints
* 1700 <= y <=2700

Ouput Format
Print the full date of Day of the Programmer during year 'y' in the format dd.mm.yy, where dd is the twho-digit day, mm 
is the two-digit month, and yyyy is 'y'

  en 1918 anho de transicion del julinao al gregoriano 14 de feb es el dia 32 de anho
  anhos se cuentan desde 1700 al 2700, un anho bisiesto en el calendario julinao es todo aquel divisible por 4;
  en el gregoriano es quel divisible por 400 o divisible por 4 pero no por 100
  en anhos bisiestos el dia del programador es el 12.03.yyyy en los demas es el 13.09.yyyy
  en 1918 el dia del programador se retraza 14 dias, luego es el 26.09.1918
 '''

def dayOfProgrammer(year:int) -> str:
    if year == 1918:
        return "26.09.1918"
    
    elif year > 1918:
        if year % 400 == 0 or (year % 4 ==0 and year % 100 != 0):
            return "12.09.{}".format(year)
        else:
            return "13.09.{}".format(year)
    else:
        if year % 4 == 0:
            return "12.09.{}".format(year)
        else:
            return "13.09.{}".format(year)


    
